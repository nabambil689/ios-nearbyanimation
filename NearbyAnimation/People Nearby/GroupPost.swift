//
//  GroupPost.swift
//  CALayerPlayground
//
//  Created by Nabil on 15/11/2017.
//  Copyright © 2017 razerware. All rights reserved.
//

import UIKit

class GroupPost: NSObject {
    
    internal var circles  : Array<PostCircle>?
    internal var center   : CGPoint
    internal var radius   : CGFloat
    internal var size_circle : CGFloat
    internal var average_distance : Double
    internal var total_distance : Double
    internal var number_circle_show : Int
    internal var hidden : Bool
    private let extendedArea = 50 as CGFloat;
    private var divisionArea = 1 as CGFloat;
    
    init(center:CGPoint, radius:CGFloat) {
        self.hidden             = false
        self.center             = center
        self.radius             = radius
        self.size_circle        = (1-(radius/center.y))*120
        self.average_distance   = 0
        self.total_distance     = 0
        self.number_circle_show = 0
        
        circles = Array<PostCircle>()
        
        super.init()
    }
    
    convenience override init() {
        self.init()
    }
    
    func createGroup(hex_type:Bool){
        
        // value for point position
        let start_point = (hex_type ? 0.0 : 90.0)
        let multiplier  = 60.0
        let end_point   = start_point + 360.0
        
        // loop for point
        for i in stride(from: start_point, to: end_point, by: multiplier){
            let rad = i * Double.pi / 180
            
            let x = Double(center.x) + Double(radius) * cos(rad)
            let y = Double(center.y) + Double(radius) * sin(rad)
            
            let circle = PostCircle(rad: rad)
            circle.setLayer(point: CGPoint(x: x, y: y), size: size_circle)
//            circle.isHidden = true
            circles?.append(circle)
        }
    }

    func updateDetails(details:[String:AnyObject], index:Int)-> PostCircle{
        let circle:PostCircle = circles![index]

        guard let url = NSURL(string: details["profile_src"] as! String) else {
            circle.isHidden = true
            return circle
        }

//        guard let distance = details["distance_meter"] as? float else {
//            circle.isHidden = true;
//            return;
//        }
//
//        total_distance += distance
        
        let image_ids =  details["image_id"] as! [[String:AnyObject]]
        var ids:[Int] = []
        for var allData in image_ids{
            let str = allData["id"]
            let image_id:Int = Int(str as! String)!
            ids.append(image_id)
        }
        
        circle.setDetail(URL: (url as URL) as URL, id: ids, follow: details["is_follow"] as! Bool)
        circle.isHidden = false
        
        return circle
    }
    
    func updateRadius(X:CGFloat,tapArea: CGFloat)-> CGFloat{
        // update radius
//        var loopCount = 1 as CGFloat;
//        let checkArea = radius+X
//
//        if (checkArea < tapArea*3){
//            while loopCount <= 3 {
//                if(checkArea <= tapArea*loopCount){
//                    break
//                }
//                divisionArea *= 1.6
//                loopCount += 1
//            }
//        }
        
//        var difference = 0 as CGFloat
//        
//        if (newX<0){
//            difference = (tapArea*loopCount) - radius
//        }else{
//            difference = radius - (tapArea*loopCount)
//        }
//        
//        
//        radius -= radius-(tapArea*loopCount)
        
        let newX = X/divisionArea
        divisionArea = 1
        radius += newX
        
        // get size circle
        size_circle =  (1-(radius/center.y))*120
        
        
        // loop for point
        for circle:PostCircle in circles!{
            
            let rad = circle.radian
            
            let x = Double(center.x) + Double(radius) * cos(rad)
            let y = Double(center.y) + Double(radius) * sin(rad)
            
            if (radius > tapArea - (tapArea/2) && size_circle > 8){
                circle.setLayer(point: CGPoint(x: x, y: y), size:size_circle)
                if (circle.opacity != 1){
                    circle.opacity = 1
                    self.hidden = false
                }
            }else{
                circle.setLayer(point: CGPoint(x: x, y: y), size:10)
                if(circle.opacity != 0 ){
                    circle.opacity = 0
                    self.hidden = true
                }
            }
        }
        
        if (radius > 0 && size_circle > 0 && radius < tapArea){
            var update_to : CGFloat = 0
            
            if (radius < tapArea)
            {update_to = tapArea - radius}
            else
            {update_to = radius - tapArea}
            
            return update_to
        }
        
        return 0
    }
    
    func checkTapArea(point:CGPoint)->PostCircle?{
        for circle in circles!{
            if (circle.contains(circle.convert(point, from: circle.superlayer))){
                return circle
            }
        }
        return nil
    }
    
}
