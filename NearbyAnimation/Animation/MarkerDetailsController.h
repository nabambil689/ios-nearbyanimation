//
//  MarkerDetailsController.h
//  ObjCDemoApp
//
//  Created by Epnox on 20/05/2017.
//  Copyright © 2017 Google. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GMUClusterItem.h"

// Details from Local Social
@interface MarkerDetails : NSObject<GMUClusterItem>

@property(nonatomic, readonly) CLLocationCoordinate2D position;
@property(nonatomic, readonly) NSString* imageID;
@property(nonatomic, readonly) NSURL  *url;

- (instancetype)initWithPosition:(CLLocationCoordinate2D)position imageID:(NSString*)imageID withURL:(NSURL*)url;

@end
