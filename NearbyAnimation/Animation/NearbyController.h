//
//  NearbyController.h
//  LocalSocial
//
//  Created by Nabil on 21/11/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarkerDetailsController.h"

@interface NearbyController : UIViewController

+ (UIImage *) imageWithView:(UIView *)view;
@property (weak, nonatomic) IBOutlet UIImageView *BackgroundImage;
@property (strong, nonatomic) UIImage *background;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
@property (strong,nonatomic) MarkerDetails *marker;
@end
