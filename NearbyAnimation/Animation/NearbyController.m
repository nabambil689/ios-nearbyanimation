//
//  NearbyController.m
//  LocalSocial
//
//  Created by Nabil on 21/11/2017.
//  Copyright © 2017 Epnox. All rights reserved.
//

#import "NearbyController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <CoreLocation/CoreLocation.h>
#import "NearbyAnimation-Swift.h"
#import <AFNetworking/AFNetworking.h>

@interface NearbyController ()
{
    NSMutableArray<GroupPost*>* Group;
    int index_second_group;
    GroupPost* tapableGroup;
    GroupPost* firstGroup;
    GroupPost* secondGroup;
    GroupPost* lastGroup;
    CGFloat    radius_gap;
    CGPoint    center;
}
@end

#define increase true
#define decrease false

@implementation NearbyController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // set background view
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.frame = self.view.bounds;
    visualEffectView.alpha = 0.7;
    
    [self.view insertSubview:visualEffectView atIndex:1];

    // set loading button
    [self.loading startAnimating];
    
    // initialize variable
    radius_gap = self.view.frame.size.width/3.3;
    center     = self.view.center;
    Group = [[NSMutableArray<GroupPost*> alloc]init];
    
    [self setupGesture];
    [self getRequest];
}

- (void) getRequest{
    //Get the url
    NSString *requestURL = @"https://api-dev.seepichere.com/v1/get_profile_by_nearest_image?session_key=2f84168496d549c%7C49683&lat=3.082654&long=101.612993&limit=200&distance=5000000";
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:requestURL];
    NSLog(@"%@", URL);
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
            if ([httpResponse statusCode] == 200){
                [self turnDataToLayer:responseObject[@"results"]];
            }else {
                NSLog(@"Error : %@",error);
            }
    }];
    
    [dataTask resume];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar viewWithTag:10].hidden = NO;
    self.tabBarController.tabBar.hidden = true;
    
    self.segment.layer.cornerRadius = 5.0f;
    self.segment.layer.masksToBounds = TRUE;
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
    // set profile photo
    //NSManagedObjectContext *context = [DataController managedObjectContext];
    //Profile *profile = [DataController fetchedEntitiedForClass:[Profile class] withPredicate:nil inManagedObjectContext:context];
    
    NSURL *url = [NSURL URLWithString:@""];
    
    [self.profileImage sd_setImageWithURL:url];
    self.profileImage.layer.cornerRadius = 50;
    self.profileImage.layer.masksToBounds = YES;
    
    self.navigationController.navigationItem.leftBarButtonItem = nil;
    self.BackgroundImage.image = self.background;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setTranslucent:YES];

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar viewWithTag:10].hidden = YES;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.navigationItem setHidesBackButton:NO animated:YES];
}

// MARK: Data Handlers
- (void) turnDataToLayer:(NSArray*)data{

    int count_image = 5;
    CGFloat group_count = 1;
    bool hex_type   = true;
    CGFloat radius  = radius_gap;
    
    GroupPost *object;

    for (NSDictionary *image_detail in data){
        // TODO : create circle group
        if (++count_image == 6){
            // Group Circle: Size of radius
            object = [[GroupPost alloc]initWithCenter:center radius:radius];
            
            // Group Circle: Type view
            hex_type = !hex_type;
            [object createGroupWithHex_type:hex_type];
            
            // Group Circle : insert to all group
            [Group addObject:object];

            count_image = 0;
//            group_count *= 1.6;
            radius  += radius_gap;// group_count;
        }
        
        // TODO : insert data to circle
        if (count_image < 6) {
            [self.view.layer addSublayer: [object updateDetailsWithDetails:image_detail index:count_image]];
        }
    }
    UIImage *image = [self roundedRectImageFromImage:self.profileImage];
    
    CALayer *layerProfileImage = [CALayer new];
    layerProfileImage.frame = self.profileImage.frame;
    layerProfileImage.contents = (__bridge id _Nullable)([image CGImage]);
    
    [self.view.layer addSublayer:layerProfileImage];
    

    if (Group.count > 0){
        firstGroup   = [Group firstObject];
        tapableGroup = [Group firstObject];
        lastGroup    = [Group lastObject];
    }
    if (Group.count > 1)
        secondGroup  = Group[1];

    [self.loading stopAnimating];
}

// MARK: Gesture Handlers
- (void) setupGesture{
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(move:)];
    pan.minimumNumberOfTouches  = 1;
    pan.maximumNumberOfTouches  = 1;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
    tap.numberOfTapsRequired    = 1;
    tap.numberOfTouchesRequired = 1;
    
    [self.view addGestureRecognizer:pan];
    [self.view addGestureRecognizer:tap];
}

- (void) move:(UIPanGestureRecognizer*) gesture{
    CGPoint translate = [gesture translationInView:self.view];
    
    BOOL flag = false;
    if (gesture.state == UIGestureRecognizerStateEnded)
        flag = true;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    
    // Calculate the movement of animation
    [self updateLayer:translate.y endedMove:flag];
    
    [UIView commitAnimations];
    
    [gesture setTranslation:CGPointZero inView:self.view];
}

- (void) tap:(UITapGestureRecognizer*) gesture
{
    CGPoint touch = [gesture locationInView:self.view];
    PostCircle *circle = [tapableGroup checkTapAreaWithPoint:touch];
    
    if (circle.image_id.count > 0)
        [self openPost:circle];
}

- (void) openPost:(PostCircle*)circle{
    NSMutableArray<MarkerDetails*> *markers = [NSMutableArray new];
    
    for (NSNumber *image_id in circle.image_id){
        NSString *str_id = [NSString stringWithFormat:@"%@",image_id];
        MarkerDetails* marker = [[MarkerDetails alloc]initWithPosition:kCLLocationCoordinate2DInvalid imageID:str_id withURL:circle.image_url];
        [markers addObject:marker];
    }
    
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//    ClusterPostCollectionController *postView = [[ClusterPostCollectionController alloc]initWithCollectionViewLayout:layout];
//
//    postView.items = markers;
//
//    [self.navigationController pushViewController:postView animated:YES];
}

// MARK: Calculation of movement the layer
- (void) updateLayer:(CGFloat)x endedMove:(BOOL)flag{
    
    for (GroupPost *bunch in Group)
    {
        CGFloat update = [bunch updateRadiusWithX:x tapArea:radius_gap];

        if (update > 0  && bunch.hidden == false )
            tapableGroup = bunch;
    }
    
    if (flag == true){
        CGFloat different = 0;
        CGFloat gap = radius_gap/2;
        CGFloat innerArea = radius_gap - gap;
        CGFloat outerArea = radius_gap + gap;
        
        if (firstGroup.radius > radius_gap)
            different = radius_gap - firstGroup.radius;
        
        else if (lastGroup.radius < radius_gap)
            different = radius_gap - lastGroup.radius ;
        
        else if (tapableGroup.radius > innerArea && tapableGroup.radius < outerArea)
            different = radius_gap - tapableGroup.radius;
            
        else if (tapableGroup.radius > outerArea)
            different = radius_gap*2 - tapableGroup.radius;

        else if(tapableGroup.radius < innerArea){
            NSInteger index = [Group indexOfObject:tapableGroup];
            if (++index < Group.count){
                GroupPost *second = Group[index];
                different = radius_gap - second.radius;
            }
        }
        
        
        if (different != 0)
            [self updateLayer:different endedMove:flag];
        
    }

}

- (void) switchGroup: (BOOL)action{
    if (secondGroup == lastGroup)
        secondGroup = nil;
    else if(action == increase)
        secondGroup = Group[++index_second_group];
    else if(action == decrease)
        secondGroup = Group[--index_second_group];
    
}

- (IBAction)segmentController:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
            
        case 1:
            break;
            
        default:
            break;
    }
}

+ (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIImage*) roundedRectImageFromImage:(UIImageView*)view{
    UIImage *image = view.image;
    
    UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0.0);
    [UIBezierPath bezierPathWithRoundedRect:view.bounds cornerRadius:(view.frame.size.height/2.0f)].addClip;
    [image drawInRect:view.bounds];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

@end
