//
//  PostCircle.swift
//  CALayerPlayground
//
//  Created by Nabil on 15/11/2017.
//  Copyright © 2017 razerware. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImage{
    
    class func roundedRectImageFromImage(image:UIImage,imageSize:CGSize,cornerRadius:CGFloat)->UIImage{
        UIGraphicsBeginImageContextWithOptions(imageSize,false,0.0)
        let bounds=CGRect(origin: .zero, size: imageSize)
        UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).addClip()
        image.draw(in: bounds)
        let finalImage=UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return finalImage!
    }
    
}

class PostCircle: CALayer {
    
    internal var profileImage : UIImage?
    internal var image_id     : [Int]?
    internal var radian       : Double
    internal var image_url    : URL?
    
    init(rad:Double) {
        // set variable
        self.radian       = rad
        
        // call super init
        super.init()
        
        // set the content
        let image = UIImage(named: "LocalSocial")
        self.contents = (UIImage.roundedRectImageFromImage(image: image!, imageSize: CGSize(width: 120, height: 120), cornerRadius: 60)).cgImage
        self.backgroundColor = UIColor.clear.cgColor
        self.contentsGravity = kCAGravityResizeAspectFill
        self.magnificationFilter = kCAFilterLinear
        
    }
    
    convenience override init() {
        self.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.radian       = 0
        super.init(coder: aDecoder)
    }
    
    override init(layer: Any) {
        self.radian = 0
        super.init(layer: layer)
    }
    
    func setDetail(URL:URL, id:[Int], follow:Bool)
    {
        image_id = id
        image_url = URL
        
        let manager = SDWebImageManager.shared()
        manager.imageDownloader?.downloadImage(with: URL, options: SDWebImageDownloaderOptions(rawValue: 0), progress: nil, completed: { (image, data, error, flag) in
            
            guard let latest = image else{
                print("error: \(String(describing: error))")
                return
            }
            
            self.profileImage = RBSquareImage(image: latest)
            
            let low_image = UIImageJPEGRepresentation(self.profileImage!, 0.05)
            let new_image = UIImage(data: low_image!)
            
            self.contents = (UIImage.roundedRectImageFromImage(image: new_image!, imageSize: CGSize(width: 120, height: 120), cornerRadius: 60)).cgImage
        })
        
        // set the border
        if (follow){
            self.borderColor = UIColor(displayP3Red: 0.082, green: 0.502, blue: 0.714, alpha: 1.0).cgColor
            self.borderWidth = 2.5
        }
    }
    
    func setLayer(point:CGPoint,size:CGFloat){
        let newpoint = CGPoint(x: point.x - size/2, y: point.y - size/2)
        self.frame = CGRect(x: newpoint.x, y: newpoint.y, width: size, height: size)
        self.cornerRadius = size/2
    }
}

func RBSquareImage(image: UIImage) -> UIImage {
    let originalWidth  = image.size.width
    let originalHeight = image.size.height
    var x: CGFloat = 0.0
    var y: CGFloat = 0.0
    var edge: CGFloat = 0.0
    
    if (originalWidth > originalHeight) {
        // landscape
        edge = originalHeight
        x = (originalWidth - edge) / 2.0
        y = 0.0
        
    } else if (originalHeight > originalWidth) {
        // portrait
        edge = originalWidth
        x = 0.0
        y = (originalHeight - originalWidth) / 2.0
    } else {
        // square
        edge = originalWidth
    }
    
    let cropSquare = CGRect(x: x, y: y, width: edge, height: edge)
    let imageRef = image.cgImage!.cropping(to: cropSquare)
    
    return UIImage(cgImage: imageRef!, scale: 1, orientation: image.imageOrientation)
}
