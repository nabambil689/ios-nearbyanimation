//
//  MarkerDetailsController.m
//  ObjCDemoApp
//
//  Created by Epnox on 20/05/2017.
//  Copyright © 2017 Google. All rights reserved.
//

#import "MarkerDetailsController.h"

@implementation MarkerDetails

- (instancetype)initWithPosition:(CLLocationCoordinate2D)position imageID:(NSString*)imageID withURL:(NSURL*)url{
    if (self = [super init]) {
        _position = position;
        _imageID  = imageID;
        _url      = url;
    }
    return self;
}

@end
