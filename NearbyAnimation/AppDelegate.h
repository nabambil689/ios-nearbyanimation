//
//  AppDelegate.h
//  NearbyAnimation
//
//  Created by Nabil on 05/03/2018.
//  Copyright © 2018 Epnox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

